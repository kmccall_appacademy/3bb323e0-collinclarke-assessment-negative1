require 'byebug'
# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result = []
  ((nums.first+1)...(nums.last)).each do |int|
    unless nums.include?(int)
      result << int
    end
  end
  result
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  base_10 = 0
  bin_arr = binary.to_s.split('').map { |str| str.to_i }
  bin_arr.each_with_index do |int, idx|
    base_10 += (int * 2**(bin_arr.length - idx - 1))
  end
  base_10
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)

    selection = Hash.new
      self.each do |key, val|
       if prc.call(key, val)
         selection[key] = val
       end
      end
    selection
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged = self.dup
       hash.each do |key1, val1|
         if merged.keys.include?(key1) && block_given?
            merged[key1] = prc.call(key1, merged[key1], val1)
         else
           merged[key1] = val1
         end
      end
  merged
  end

end




# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
 lucas = [2, 1]
 num = n.abs
 idx = 2
  until lucas[num] || idx == (num + 1)
    lucas[idx] = (lucas[idx - 1] + lucas[idx - 2])
    idx += 1
  end
  return lucas[n] unless n < 0
  num.even? ? lucas[num] : (0 - lucas[num])
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  first = 0
  palindromes = []
  while !string[first..-1].empty?
   last = -1
   while !string[first..last].empty?
     word = string[first..last]
     if palindrome?(word)
       palindromes << word
     end
    last -= 1
   end
   first += 1
 end
 palindromes.sort_by!{|word| word.length}
 unless palindromes.last.length < 3
   return palindromes.last.length
 end
 false
end

def palindrome?(word)
  word.reverse == word
end

# def longest_palindrome(string)
#   palindromes = []
#   letters = string.chars
#   letters.each_with_index do |ch, idx|
#     next if idx == 0
#     if letters[idx+1] == letters[idx-1]
#       center = idx
#       n = 1
#       until letters[idx+n] != letters[idx-n]
#         n += 1
#       end
#         n -= 1
#       palindromes << string[(idx-n)..(idx+n)]
#     end
#   end
#
#   unless palindromes.empty?
#    return palindromes.sort_by{|word| word.length}.last.length
#   end
#   false
#
# end
